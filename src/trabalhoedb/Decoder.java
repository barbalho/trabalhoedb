package trabalhoedb;

import java.util.ArrayList;

/**
 *
 * @author Felipe Barbalho Rocha, Jackson Rauup Silvestre Santos , Raul Silveira Silva
 */
public class Decoder {
    public static final String ENESIMO = "ENESIMO"; 
    public static final String POSICAO = "POSICAO";
    public static final String REMOVA = "REMOVA"; 
    
    public static final String MEDIANA = "MEDIANA"; 
    public static final String CHEIA = "CHEIA"; 
    public static final String COMPLETA = "COMPLETA"; 
    public static final String IMPRIMA = "IMPRIMA"; 
     public static final String INSIRA = "INSIRA";
    
    /**
     * Método principal de execução
     * @param args 
     */
    public static void main(String[] args) {
        //Cria a arvore
        BinarySearchTree ABB = new BinarySearchTree();
         
        //ler os elementos do arquivo para armazenar na arvore
        ArrayList<Integer> listaDeElementos = elementosDoArquivo();
        
        //popula a arvore com os elementos lidos
        popularArvore(ABB, listaDeElementos);
        
        //ler os comandos, para serem executados na arvore
        ArrayList<String> comandos = FileManager.lerArquivo(FileManager.ARQUIVO_DE_COMANDOS);
        
        //Executa os comandos
        for (String comando : comandos) {
            executarComando(ABB, comando);
        }
        
    }
     
    /**
     * Retorna os elementos do arquivo para armazenar na arvore
     * @return os elementos do arquivo para armazenar na arvore
     */
    public static ArrayList<Integer> elementosDoArquivo(){
        return FileManager.lerElementosDeArquivo(FileManager.ARQUIVO_ABB, " ");
    }
     
    /**
     * popula a arvore com os elementos lidos
     * @param ABB Arvare a ser populada.
     * @param listaDeElementos arrayList de elementos inteiros
     * @return verdadeiro se funcionar
     */
    public static boolean popularArvore(BinarySearchTree ABB, ArrayList<Integer> listaDeElementos){
        for (Integer elementoDaArvore : listaDeElementos) {
            ABB.inserir(elementoDaArvore);
        }
        return true;
    }
     
    /**
     * Executa os comandos na arvore
     * @param ABB a ser trabalhada
     * @param comando comando a ser executado na arvore
     * @return 
     */
    public static boolean executarComando(BinarySearchTree ABB, String comando){
        NodeBinaryTree novoNo = new NodeBinaryTree(null);
        
        String operacao[] = comando.split(" ");
        if(operacao.length==1){
            System.out.println(operacao[0]);
            if(operacao[0].equalsIgnoreCase(MEDIANA)){
                System.out.println("--- " + ABB.mediana());
                ABB.mediana();
            }else if(operacao[0].equalsIgnoreCase(CHEIA)){
                System.out.println("--- " + ABB.ehCheia());
            }else if(operacao[0].equalsIgnoreCase(COMPLETA)){
                System.out.println("--- " + ABB.ehCompleta());
            }else if(operacao[0].equalsIgnoreCase(IMPRIMA)){
                System.out.println("--- " + ABB.toString());
            }
        }else if(operacao.length==2){
            System.out.println(operacao[0] + " " + operacao[1]);
            try{
                if(operacao[0].equalsIgnoreCase(ENESIMO)){
                    System.out.println("--- " +  ABB.enesimoElemento(Integer.parseInt(operacao[1])));  
                }else if(operacao[0].equalsIgnoreCase(POSICAO)){
                    System.out.println("--- " + ABB.posicao(Integer.parseInt(operacao[1])));
                }else if(operacao[0].equalsIgnoreCase(REMOVA)){
                    boolean response = ABB.remover(Integer.parseInt(operacao[1]));
                    if(response){
                        System.out.println("--- " + ABB.toString());
                    }
                }else if(operacao[0].equalsIgnoreCase(INSIRA)){
                    boolean response = ABB.inserir(Integer.parseInt(operacao[1]));
                    if(response){
                        System.out.println("--- " + ABB.toString());
                    }
                }
            }catch(Exception erro){
                System.out.println("[EXCEPTION] paramentro '"+operacao[1]+"' inválido na Operação "+operacao[0]);
            }
        }else{
            System.err.println("Erro no comando");
        }
        return false; 
    }
    
    static void testeQuantidadeElementos(BinarySearchTree ABB){
        System.out.println("--------------------------------------");
        System.out.println(ABB.buscar(50).getTamanhoEsquerda() + " - " + ABB.buscar(50).getChave() + " - " + ABB.buscar(50).getTamanhoDireita());
        System.out.println(ABB.buscar(15).getTamanhoEsquerda() + " - " + ABB.buscar(15).getChave() + " - " + ABB.buscar(15).getTamanhoDireita());
        System.out.println(ABB.buscar(40).getTamanhoEsquerda() + " - " + ABB.buscar(40).getChave() + " - " + ABB.buscar(40).getTamanhoDireita());
        System.out.println(ABB.buscar(20).getTamanhoEsquerda() + " - " + ABB.buscar(20).getChave() + " - " + ABB.buscar(20).getTamanhoDireita());
        System.out.println(ABB.buscar(60).getTamanhoEsquerda() + " - " + ABB.buscar(60).getChave() + " - " + ABB.buscar(60).getTamanhoDireita());
        System.out.println(ABB.buscar(10).getTamanhoEsquerda() + " - " + ABB.buscar(10).getChave() + " - " + ABB.buscar(10).getTamanhoDireita());
        System.out.println(ABB.buscar(30).getTamanhoEsquerda() + " - " + ABB.buscar(30).getChave() + " - " + ABB.buscar(30).getTamanhoDireita());
        System.out.println(ABB.buscar(55).getTamanhoEsquerda() + " - " + ABB.buscar(55).getChave() + " - " + ABB.buscar(55).getTamanhoDireita());
        System.out.println(ABB.buscar(80).getTamanhoEsquerda() + " - " + ABB.buscar(80).getChave() + " - " + ABB.buscar(80).getTamanhoDireita());
        System.out.println(ABB.buscar(15).getTamanhoEsquerda() + " - " + ABB.buscar(15).getChave() + " - " + ABB.buscar(15).getTamanhoDireita());
        System.out.println(ABB.buscar(90).getTamanhoEsquerda() + " - " + ABB.buscar(90).getChave() + " - " + ABB.buscar(90).getTamanhoDireita());
        System.out.println("--------------------------------------");
    }
}