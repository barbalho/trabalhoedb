package trabalhoedb;

/**
 *
 * @author Felipe Barbalho Rocha, Jackson Rauup Silvestre Santos , Raul Silveira Silva
 */
public interface TreeOperations {
    
    /**
     * 
     * @param chave
     * @return 
     */
    abstract NodeBinaryTree buscar(Integer chave);
    
    /**
     * 
     * @param valor
     * @return 
     */
    abstract boolean inserir(Integer valor);
    
    /**
     * 
     * @param valor
     * @return 
     */
    abstract boolean remover(Integer valor);
}
