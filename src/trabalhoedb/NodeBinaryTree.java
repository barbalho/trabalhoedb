package trabalhoedb;

/**
 *
 * @author Felipe Barbalho Rocha, Jackson Rauup Silvestre Santos , Raul Silveira Silva
 */
public class NodeBinaryTree {
    
    private NodeBinaryTree esquerda;
    private int tamanhoEsquerda;
    private NodeBinaryTree direita;
    private int tamanhoDireita;
    
    private Integer chave;

    public NodeBinaryTree() {
        this.esquerda = null;
        this.direita = null;
        this.tamanhoEsquerda = 0;
        this.tamanhoDireita = 0;
        this.chave = null;
    }
    
    public NodeBinaryTree(Integer chave) {
        this.esquerda = null;
        this.direita = null;
        this.tamanhoEsquerda = 0;
        this.tamanhoDireita = 0;
        this.chave = chave;
    }

    public NodeBinaryTree getEsquerda() {
        return esquerda;
    }

    public void setEsquerda(NodeBinaryTree esquerda) {
        this.esquerda = esquerda;
    }

    public NodeBinaryTree getDireita() {
        return direita;
    }

    public void setDireita(NodeBinaryTree direita) {
        this.direita = direita;
    }

    public int getTamanhoEsquerda() {
        return tamanhoEsquerda;
    }

    public void setTamanhoEsquerda(int tamanhoEsquerda) {
        this.tamanhoEsquerda = tamanhoEsquerda;
    }

    public int getTamanhoDireita() {
        return tamanhoDireita;
    }

    public void setTamanhoDireita(int tamanhoDireita) {
        this.tamanhoDireita = tamanhoDireita;
    }

    public Integer getChave() {
        return chave;
    }

    public void setChave(Integer chave) {
        this.chave = chave;
    }
    
    @Override
    public String toString() {
        return "Nó{" + "esquerda=" + esquerda + ", tamanhoEsquerda=" + tamanhoEsquerda + ", direita=" + direita + ", tamanhoDireita=" + tamanhoDireita + '}';
    }
}