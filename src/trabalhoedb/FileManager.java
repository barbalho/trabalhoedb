package trabalhoedb;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Felipe Barbalho Rocha, Jackson Rauup Silvestre Santos , Raul Silveira Silva
 */
public class FileManager {
    public static final String ARQUIVO_ABB = "files/arquivo  de entrada da ABB.txt";
    public static final String ARQUIVO_DE_COMANDOS = "files/arquivo de comandos.txt";
    public static final String FALHA_LEITURA_MENSAGEM = "Falha ao tentar ler arquivo";
    
    /**
     * Lêr um arquivo e retorna as linhas em um ArrayList, em caso de falha o 
     * retorno e nulo
     * @param filePath endereço do arquivo
     * @return linhas em um ArrayList, ou null em caso de falha
     */
    public static ArrayList<String> lerArquivo(String filePath){
        ArrayList<String> listaDeLinhas = new ArrayList<>();
        try{  
            BufferedReader br = new BufferedReader(new FileReader(filePath));  
            while(br.ready()){  
                String linha = br.readLine().trim();  
                if(!linha.equals("")){
                    listaDeLinhas.add(linha);
                }
                    
            }  
            br.close();  
        }catch(IOException ioe){  
            System.err.println(FALHA_LEITURA_MENSAGEM);
            return null;
        }  
        return listaDeLinhas;
    }
    
    /**
     * Lêr um arquivo e retorna os elementos em um ArrayList, splitParam define 
     * o ponto de separação dos elementos 
     * @param filePath endereço do arquivo
     * @param splitParam padão de para separação dos elementos
     * @return ArrayList de interiros, ou null em caso de falha
     */
    public static ArrayList<Integer> lerElementosDeArquivo(String filePath, String splitParam){
        ArrayList<Integer> itens = new ArrayList<>();
        try{  
            BufferedReader br = new BufferedReader(new FileReader(filePath));  
            while(br.ready()){  
                String linha = br.readLine().trim();  
                if(!linha.equals("")){
                    String array[] = linha.split(splitParam);
                    for (String elemento : array) {
                        itens.add(Integer.parseInt(elemento.trim()));
                    }
                } 
            }  
            br.close();  
        }catch(IOException ioe){  
            System.err.println(FALHA_LEITURA_MENSAGEM);
            return null;
        }  
        return itens;
    }
}
