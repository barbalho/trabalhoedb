package trabalhoedb;

import java.util.ArrayList;

/**
 *
 * @author Felipe Barbalho Rocha, Jackson Rauup Silvestre Santos , Raul Silveira Silva
 */
public class BinarySearchTree implements TreeOperations{
    
    private NodeBinaryTree raiz;

    /**
     * Construtor
     */
    public BinarySearchTree() {
        this.raiz = new NodeBinaryTree();
    }
    
    /**
     * retorna o n-ésimo elemento (contando a partir de 1) do percurso em ordem
     * (ordem simétrica) da ABB.
     *
     * @param n index
     * @return n-ésimo elemento
     */
    public Integer enesimoElemento(int n) {
        NodeBinaryTree aux = raiz;
        
        int left = aux.getTamanhoEsquerda();
        boolean right = false; //Serve pra ver se andou primeiro para D e depois E
        while(true){
            if(n == left+1){
                return aux.getChave();
            }else if(n < left+1){
                aux = aux.getEsquerda();
                if(right){
                    left += aux.getTamanhoEsquerda()-1;
                }else{
                    left = aux.getTamanhoEsquerda();
                }
                right = false;
            }else if(n > left+1){
                right = true;
                aux = aux.getDireita();

                left += aux.getTamanhoEsquerda()+1;
            }
        }
    }
    
    /**
     * Retornar a quantidade de elementos da ABB.
     *
     * @return quantidade de elementos.
     */
    public int getQtdElementos() {
        return (getRaiz().getTamanhoEsquerda() + getRaiz().getTamanhoDireita()) + 1;
    }
    
    /**
     * retorna a posição ocupada pelo elemento x em um percurso em ordem 
     * simétrica na ABB (contando a partir de 1).
     * @param x elemento da árvore.
     * @return posição ocupada pelo elemento x em um percurso em ordem simétrica.
     */
    public int posicao(Integer x){
        NodeBinaryTree aux = raiz;
        
        int left = aux.getTamanhoEsquerda();
        boolean right = false; //Serve pra ver se andou primeiro para D e depois E
        while(true){
            if(x == aux.getChave()){
                return left+1;
            }else if(x < aux.getChave()){
                aux = aux.getEsquerda();
                if(right){
                    left += aux.getTamanhoEsquerda()-1;
                }else{
                    left = aux.getTamanhoEsquerda();
                }
                right = false;
            }else if(x > aux.getChave()){
                right = true;
                aux = aux.getDireita();
                left += aux.getTamanhoEsquerda()+1;
            }
        }
    }
    
    /**
     * retorna o elemento que contém a mediana da ABB. Se a ABB  possuir um 
     * número par de elementos, retorne o menor dentre os dois elementos medianos.
     * @return elemento que contém a mediana da ABB.
     */
    public Integer mediana(){
        NodeBinaryTree aux = raiz;
        
        int right = aux.getTamanhoDireita();
        int left = aux.getTamanhoEsquerda();
       
        while(aux!=null){
            int desequilibrio = left-right;
        
            if(desequilibrio == 0 || desequilibrio == -1){
                return aux.getChave();
            }else if(desequilibrio == 1){
                return aux.getEsquerda().getChave();
            }else if(desequilibrio > 1){
                right++;
                left--;
                aux = aux.getEsquerda();
            }else if(desequilibrio < -1){
                right--;
                left++;
                aux = aux.getDireita();
            }
        }
        return null;
    }
    
    /**
     * Retorna verdadeiro se a ABB for uma árvore binária cheia e falso, caso 
     * contrário.
     * @return se é árvore binária cheia.
     */
    public boolean ehCheia(){
        return ehCheia(raiz);
    }
    
    /**
     * Retorna verdadeiro se a aux for uma árvore binária cheia e falso, caso 
     * contrário.
     * @param node node para verificar a propriedade
     * @return se é árvore binária cheia.
     */
    private boolean ehCheia(NodeBinaryTree node){
        
        if(node == null){
            return true;
        }if(node.getTamanhoEsquerda() == node.getTamanhoDireita() ){
            return ehCheia(node.getEsquerda()) && ehCheia(node.getDireita());
        }else{
            return false;
        }
    }
    
    /**
     * Retorna  verdadeiro  se  a  ABB  for  uma  árvore  binária completa.
     * @return se é árvore  binária completa.
     */
    public boolean  ehCompleta(){
        return ehCompleta(raiz);
    } 
    
    /**
     * Retorna verdadeiro se a aux for uma árvore binária Completa e falso, caso 
     * contrário.
     * @param node node para verificar a propriedade
     * @return se é árvore binária Completa.
     */
    private boolean ehCompleta(NodeBinaryTree node){
        if(node == null || node.getEsquerda() == null && node.getDireita() == null){
            return true;
        }else if(node.getEsquerda() != null && node.getDireita() != null){
            return ehCompleta(node.getEsquerda()) && ehCompleta(node.getDireita());
        }else if((node.getEsquerda() == null && node.getDireita().getTamanhoDireita()==0 && node.getDireita().getTamanhoEsquerda()==0)
            || (node.getDireita()== null && node.getEsquerda().getTamanhoEsquerda()==0 && node.getEsquerda().getTamanhoDireita()==0 )){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Retorna  uma  String  que  contém  a  sequência  de  visitação 
     * (percorrimento) da ABB por nível.
     * @return String  que  contém  a  sequência  de  visitação por nível.
     */
    @Override
    public String toString(){
        NodeBinaryTree pt = raiz;
        ArrayList<NodeBinaryTree> fila = new ArrayList<>();
        String tree = "";
        int h = 0;
        int n = 1;
        fila.add(pt);
        while (n > h) {
           pt = fila.get(h++);
           tree+= pt.getChave()+" ";
           if (pt.getEsquerda() != null){
                fila.add(n++, pt.getEsquerda());
           }
           if (pt.getDireita() != null){
               fila.add(n++,pt.getDireita());
           }
        }
        return tree;
    } 
    
    /**
     * Retorna  uma  String  que  contém  a  sequência  de  visitação Em ordem 
     * simetrica
     * @param tree arvore retornada.
     * @param pt raiz da arvore
     * @return String  que  contém  a  sequência  de  visitação em ordem.
     */
    private String treeSimetricOrder(String tree, NodeBinaryTree pt){
        if (pt != null) { 
            tree+=treeSimetricOrder("", pt.getEsquerda()); 
            tree+=pt.getChave()+" ";
            tree+=treeSimetricOrder("", pt.getDireita()); 
        }else{
            return "";
        }
        return tree;
    }

    /**
     * Procura um nó com uma chave específica
     * @param chave Objeto do tipo Integer que se deseja ser buscado
     * @return NodeBinartTree com da chave especificada.
     */
    @Override
    public NodeBinaryTree buscar(Integer chave) {
        NodeBinaryTree p = raiz;
        if(p.getChave() == null){
            return null;
        }
        do{
           if(p.getChave().equals(chave)){
               return p;
           }else if(chave < p.getChave()){
                if(p.getEsquerda() == null){
                    return null;
                }else{
                    p = p.getEsquerda();
                }
           }else{
                if(p.getDireita() == null){
                    return null;
                }else{
                    p = p.getDireita();
                }
           }
        }while(true);
    }
    

    /**
     * Insere na árvore um novo elemento
     * @param valor Objeto do tipo Integer a ser inserido
     * @return Valor booleano informando se o elemento foi inserido ou não.
     */
    @Override
    public boolean inserir(Integer valor) {
        NodeBinaryTree p = raiz;
        if(buscar(valor) != null){
            return false;
        }
        if(p.getChave() == null){
            p.setChave(valor);
        }else{
            inserir(p, valor);
        }
        return true;
    }
    
    /**
     * Insere na árvore um novo elemento
     * @param valor Objeto do tipo Integer a ser inserido
     * @return Valor booleano informando se o elemento foi inserido ou não.
     */
    private int inserir(NodeBinaryTree p, Integer valor){
        while(true){
            if(valor > p.getChave()){
                p.setTamanhoDireita(p.getTamanhoDireita()+1);
                if(p.getDireita()==null){
                    p.setDireita(new NodeBinaryTree(valor));
                    return valor;
                }else{
                    p = p.getDireita();
                }
            }else if(valor < p.getChave()){
                p.setTamanhoEsquerda(p.getTamanhoEsquerda()+1);
                if(p.getEsquerda()==null){
                    p.setEsquerda(new NodeBinaryTree(valor));
                    return valor;
                }else{
                    p = p.getEsquerda();
                }
            }
        }
    }

    /**
     * Busca pai para o processo de remoção, ele já ajusta a contagem da 
     * quantidade de elementos da sub-arvore.
     * @param chave chave do elemento que ira buscar o pai.
     * @return pai do elemento com chave ou null caso seja raiz.
     */
    public NodeBinaryTree buscarPai(Integer chave) {
        NodeBinaryTree p = raiz;
        if(p.getChave() == null || p.getChave().equals(chave)){
            return null;
        }
        do{
           if(p.getEsquerda() != null && p.getEsquerda().getChave().equals(chave)){
               p.setTamanhoEsquerda(p.getTamanhoEsquerda()-1);
               return p;
           }
           if(p.getDireita()!= null && p.getDireita().getChave().equals(chave)){
               p.setTamanhoDireita(p.getTamanhoDireita()-1);
               return p;
           }else if(chave < p.getChave()){
                if(p.getEsquerda() == null){
                    return null;
                }else{
                    p.setTamanhoEsquerda(p.getTamanhoEsquerda()-1);
                    p = p.getEsquerda();
                }
           }else{
                if(p.getDireita() == null){
                    return null;
                }else{
                    p.setTamanhoDireita(p.getTamanhoDireita()-1);
                    p = p.getDireita();
                }
           }
        }while(true);
    }
    
    /**
     * Remove o nó com chave = valor
     * @param valor chave do nó
     * @return true, caso seja removido algum valor
     */
    @Override
    public boolean remover(Integer valor) {  
        NodeBinaryTree no = buscar(valor);
        NodeBinaryTree pai = buscarPai(valor);
        if(no != null){
            if(no.getTamanhoDireita()==0 && no.getTamanhoEsquerda()==0){
                return removerCaso1(no, pai);
            }else if(no.getTamanhoDireita()==0 || no.getTamanhoEsquerda()==0){
                return removerCaso2(no, pai);
            }else{
                return removerCaso3(no, pai);
            }
        }
        return true;
    }
    
    /**
     * Primeiro caso de remoção: Deseja-se remover uma chave que está numa folha.
     * @param no nó a ser removido
     * @param pai pai do nó, null caso o nó seja raiz
     * @return true em caso de sucesso
     */
    public boolean removerCaso1(NodeBinaryTree no, NodeBinaryTree pai){
        if(pai.getDireita() != null && pai.getDireita().equals(no)){
            pai.setDireita(null);
        }else if(pai.getEsquerda()!= null && pai.getEsquerda().equals(no)){
            pai.setEsquerda(null);
        }else{
            return false;
        }
        return true;
    }
    
    /**
     * Segundo caso de remoção: A chave removida não é uma folha, mas possui uma 
     * subárvore vazia.
     * @param no nó a ser removido
     * @param pai pai do nó, null caso o nó seja raiz
     * @return true em caso de sucesso
     */
    public boolean removerCaso2(NodeBinaryTree no, NodeBinaryTree pai){
        NodeBinaryTree novoNo = null;
        if(no.getDireita()!=null){
            novoNo = no.getDireita();
        }else if(no.getEsquerda()!=null){
            novoNo = no.getEsquerda();
        }else{
            return false;
        }
        if(pai.getDireita().equals(no)){
            pai.setDireita(novoNo);
        }else if(pai.getEsquerda().equals(no)){
             pai.setEsquerda(novoNo);
        }else{
            return false;
        }
        return true;
    }
    
    /**
     * Terceiro caso de remoção: A chave removida não é uma folha e possui duas 
     * subárvores não vazias.
     * @param no nó a ser removido
     * @param pai pai do nó, null caso o nó seja raiz
     * @return true em caso de sucesso
     */
    public boolean removerCaso3(NodeBinaryTree no, NodeBinaryTree pai){
        NodeBinaryTree novoNo = no.getEsquerda();
        NodeBinaryTree aux = null;
        while(novoNo.getDireita() != null){
            novoNo.setTamanhoDireita(novoNo.getTamanhoDireita()-1);
            aux = novoNo;
            novoNo = novoNo.getDireita();
        }
        
        novoNo.setTamanhoEsquerda(no.getTamanhoEsquerda()-1);
        novoNo.setTamanhoDireita(no.getTamanhoDireita());
        if(aux != null){
            aux.setDireita(novoNo.getEsquerda());
        }
        if(!no.getEsquerda().equals(novoNo)){
            novoNo.setEsquerda(no.getEsquerda());
        }else{
            novoNo.setEsquerda(null);
        }
        novoNo.setDireita(no.getDireita());
        if(pai != null){
            if(pai.getDireita().equals(no)){
                pai.setDireita(novoNo);
            }else if(pai.getEsquerda().equals(no)){
                 pai.setEsquerda(novoNo);
            }
        }else{
            raiz = novoNo;
        }
        return true;
    }

    /**
     * get Árvore
     * @return árvore
     */
    public NodeBinaryTree getRaiz() {
        return raiz;
    }

    /**
     * seta new arvore
     * @param raiz new arvore
     */
    public void setRaiz(NodeBinaryTree raiz) {
        this.raiz = raiz;
    }
}