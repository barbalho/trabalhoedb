﻿Componentes: Felipe Barbalho Rocha, Jackson Rauup Silvestre Santos , Raul Silveira Silva

1.	Dependências:
	a.	JDK 8 [1.8.0_25]
2.	Editar os parâmetros da aplicação:
	a.	Acessa a pasta /dist
	b.	Edita o arquivo ‘arquivo de entrada da ABB.txt’ para alterar os elementos inicias da árvore.
	c.	Edita o arquivo ‘arquivo de comandos.txt’ para inserir comandos que serão executados pela aplicação, Como mostrado a seguir:

3.	Executar o projeto:
	a.	Acessa a pasta /dist
	b.	Executa o arquivo jar TrabalhoEDB.jar
	c.	Ou com dois cliques ou pelo comando: java -jar TrabalhoEDB.jar

4.	Informações gerais:
	a.	O projeto foi desenvolvido no NetBeans 
	b.	Se o projeto for reconstruído na IDE, é necessário copiar a pasta files,  para a pasta dist, para executar TrabalhoEDB.jar


5.	Observação:
	a. 	É importante manter o executavel jar no mesmo diretório que a pasta files!
